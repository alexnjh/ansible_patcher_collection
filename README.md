# Ansible Collection - automation.patcher

## :page_facing_up: Contents
- [Contents](#contents)
  - [Description](#desc)
  - [Supported Devices](#supported_list)
  - [Installing the collection](#install)

<br>

<a name="desc"/></a>
### 1. Description

This ansible collection contains roles designed for uploading new firmware and patching of different network equipment from different vendors.
Please refer to **[Supported Devices](#supported_list)** for a list of supported network equipment.

<br>

<a name="supported_list"/></a>
### 2. Supported devices

| Vendor | Device Type                     | Role name                                                                                                                                                                |
|--------|---------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Cisco  | Cisco Catalyst switches/routers | [automation.patcher.ios.push_firmware](https://gitlab.com/alexnjh/ansible_patcher_collection/-/blob/master/docs/ios/push_firmware.md) **(For uploading new firmware to device)**<br>[automation.patcher.ios.patch_firmware](https://gitlab.com/alexnjh/ansible_patcher_collection/-/blob/master/docs/ios/patch_firmware.md) **(For patching the device with the new firmware)**<br>[automation.patcher.ios.clean_switch](https://gitlab.com/alexnjh/ansible_patcher_collection/-/blob/master/docs/ios/clean_switch.md) **(For cleaning up unused files in the switch)**<br>[automation.patcher.ios.rollback_firmware](https://gitlab.com/alexnjh/ansible_patcher_collection/-/blob/master/docs/ios/rollback_firmware.md) **(For rolling back a faulty firmware)**   |
| Cisco  | Cisco Nexus switches            | [automation.patcher.nxos.push_firmware](https://gitlab.com/alexnjh/ansible_patcher_collection/-/blob/master/docs/nxos/push_firmware.md) **(For uploading new firmware to device)**<br>[automation.patcher.nxos.patch_firmware](https://gitlab.com/alexnjh/ansible_patcher_collection/-/blob/master/docs/nxos/patch_firmware.md) **(For patching the device with the new firmware)<br>** |

<br>

<a name="install"/></a>
### 3. Installing the collection

It is recommanded to install using the ansible-galaxy tool.

```
    Installing the collection using ansible-galaxy and git clone (Ansible 2.9 and below)

      git clone https://gitlab.com/alexnjh/ansible_patcher_collection.git
      cd ansible_patcher_collection
      ansible-galaxy collection build && ansible-galaxy collection install *.tar.gz
      rm -rf *.tar.gz
      rm -rf ansible_patcher_collection      

```

<br>
