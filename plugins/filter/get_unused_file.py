from ansible.utils.display import Display

class FilterModule(object):
    def filters(self): return {'get_unused_file_to_list': self.get_unused_file_to_list}

    def get_unused_file_to_list(self, message, **kwargs):

        list_of_strings = message.splitlines()

        for index, line in enumerate(list_of_strings):
            if "The following files will be deleted" in line:
                return self.process_list_of_files(list_of_strings[index+1:])

        return []

    def process_list_of_files(self, list_of_filenames):

        result = list()

        for filename in list_of_filenames:
            if filename.startswith("/"):
                temp = filename.split('/')
                result.append("{}:/{}".format(temp[1],temp[-1]))    

        return result

