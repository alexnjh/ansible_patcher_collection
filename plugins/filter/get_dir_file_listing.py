from ansible.utils.display import Display

class FilterModule(object):
    def filters(self): return {'get_dir_file_mapping': self.get_dir_file_mapping}

    def get_dir_file_mapping(self, message, target="", type="ios", **kwargs):


        curr_dir=""
        result={}

        list_of_strings = message.splitlines()

        if type == "ios":

            for index, line in enumerate(list_of_strings):

                if "Directory of" in line:
                    curr_dir = self.remove_duplicated_forward_slash(line.split()[-1])
                    result[curr_dir]=[]
                    continue

                if "bytes" not in line and line != "":
                    result[curr_dir].append(line.split()[-1])


            if target != "":


                for key, value in result.items():

                    if target in value:
                        return {key:[target]}

                return {}

        elif type == "nxos":

            temp=[]

            for index, line in enumerate(list_of_strings):

                if "Usage for" in line:
                    curr_dir = self.remove_duplicated_forward_slash(line.split()[-1])
                    result[curr_dir]=temp
                    temp=[]
                    continue

                if "bytes" not in line and line != "":
                    temp.append(line.split()[-1])


            if target != "":


                for key, value in result.items():

                    if target in value:
                        return {key:[target]}

                return {}


        return result

    def remove_duplicated_forward_slash(self, str):

        p = ""

        for char in str:
            if char == '/' and '/' in p:
                continue
            else:
                p=p+char

        return str
