from ansible.utils.display import Display

class FilterModule(object):
    def filters(self): return {'get_switch_stack_listing': self.get_switch_stack_listing}

    def get_switch_stack_listing(self, message, **kwargs):

        result = []

        # Switch number | Role | Mac | Priority | HW Version | Current State

        for line in message.splitlines():

            temp = line.split()
            master = False

            if temp[0].startswith('*'):
                temp[0] = temp[0].replace('*','')
                master = True

            if isnumeric(temp[0].replace('*','')):

                switch_entry = {
                "id": temp[0],
                "state": temp[-1],
                "master": master
                }

                result.append(switch_entry)

        return result
