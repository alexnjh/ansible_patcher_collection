automation.patcher.nxos.patch_firmware
=========

This role contains all the relevant tasks for patching Cisco Nexus switches with new firmware that is stored locally.

Requirements
------------

- Ansible version 2.8 and above.

Role Variables
--------------

| **Variable**            	| **Description**                                                                                                                                                                                                                                	| **Data type** 	| **Avaliable options**           	| **Default value** 	|
|-------------------------	|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|---------------	|---------------------------------	|-------------------	|
| operation_mode          	| Set the operation mode of the script. <br><br>**AUTO MODE:** Ansible will automatically search for new firmware to push<br><br>**MANUAL MODE:** Firmware name will be defined by the admin and Ansible to proceed to upload it                 	| string        	| - auto<br>- manual              	| auto              	|
| hash_file_location      	| The path to the directory containing the hash files for verifiying the new firmwares                                                                                                                                                           	| string        	|                                 	| \<firmware_dir\>  	|
| hash_file_name          	| The name of the hash file<br><br>If not specified Ansible will attempt to find a .txt file that has the same name as the firmware it is intending to upload                                                                                    	| string        	|                                 	| ""                	|
| reboot_after_patch      	| Set this to true if you would like Ansible to restart the switch after patching<br><br>If set to false, the user would have to restart the switch manually to ensure the patch is successful                                                   	| boolean       	| - true<br>- false               	| true              	|
| disable_checks          	| Disable verification checks and just proceed with patching routine.<br><br>**DO NOT SET THIS TO TRUE UNLESS ABSOLUTELY NECESSARY !!!**                                                                                                         	| boolean       	| - true<br>- false               	| false             	|
| new_firmware_name       	| The name of the firmware to patch the switch with<br><br>This variable is only utilized if **operation_mode = manual**                                                                                                                         	| string        	|                                 	| ""                	|
| new_firmware_hash       	| The hash of the firmware specified in new_firmware_name<br><br>This variable is only utilized if **operation_mode = manual**                                                                                                                   	| string        	|                                 	| ""                	|
| ansible_command_timeout 	| A magic variable in ansible that sets the duration to wait in seconds before terminating a command.<br><br>Set this higher if facing timeout errors.                                                                                           	| int           	|                                 	| 3200              	|
| firmware_dir       	| The path to the directory containing the new firmware                                                                                                                                                                         	| string    	|                    	| firmware                                                         	|

Dependencies
------------

  - scp version 0.10.2 and above (https://pypi.org/project/scp)

Example Playbook
----------------

```
- name: Patch cisco nexus switches based on their model numbers
  hosts: all
  gather_facts: false
  vars:
    ansible_network_os: nxos

  tasks:

    - name: Get switch details
      nxos_facts:

    - include_role:
        name: automation.patcher.nxos.patch_firmware
        apply:
          vars:
            firmware_dir: "{{ playbook_dir }}/firmware/{{ ansible_net_model }}"          
            ansible_command_timeout: 3200
```

License
-------

BSD

Author Information
------------------

Alex Neo Jing Hui, Contact me at me@alexneo.net
