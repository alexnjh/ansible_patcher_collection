automation.patcher.ios.rollback_firmware
=========

This role contains all the relevant tasks for rolling back of firmware to Cisco Catalyst switches

Requirements
------------

- Ansible version 2.8 and above.

Role Variables
--------------

| **Variable**            	| **Description**                                                                                                                                      	| **Data type** 	| **Avaliable options** 	| **Default value** 	|
|-------------------------	|------------------------------------------------------------------------------------------------------------------------------------------------------	|---------------	|-----------------------	|-------------------	|
| ansible_command_timeout 	| A magic variable in ansible that sets the duration to wait in seconds before terminating a command.<br><br>Set this higher if facing timeout errors. 	| int           	|                       	| 3200              	|                                                      	|

Dependencies
------------


Example Playbook
----------------

```
- name: Roll back firmware in cisco switches 
  hosts: all
  gather_facts: false
  vars:
    ansible_network_os: ios

  tasks:
  
    - include_role:
        name: automation.patcher.ios.rollback_firmware
        apply:
            become: true
```

License
-------

BSD

Author Information
------------------

Alex Neo Jing Hui, Contact me at me@alexneo.net


