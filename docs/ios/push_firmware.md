automation.patcher.ios.push_firmware
=========

This role contains all the relevant tasks for uploading/pushing of firmware to Cisco Catalyst switches

Requirements
------------

- Ansible version 2.8 and above.

Role Variables
--------------
| Variable           	| Description                                                                                                                                                                                                                   	| Data type 	| Avaliable options  	| Default value                                                    	|
|--------------------	|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|-----------	|--------------------	|------------------------------------------------------------------	|
| operation_mode     	| Set the operation mode of the script. <br><br>**AUTO MODE:** Ansible will automatically search for new firmware to push<br><br>**MANUAL MODE:** Firmware name will be defined by the admin and Ansible to proceed to upload it 	| string    	| - auto<br>- manual 	| auto                                                             	|
| scp_timeout        	| The duration in seconds to wait for upload to complete.<br><br>Set this to a relatively high number if the files are more than 400 MB                                                                                         	| int       	|                    	| 3600                                                             	|
| firmware_dir       	| The path to the directory containing the new firmware                                                                                                                                                                         	| string    	|                    	| firmware                                                         	|
| hash_file_location 	| The path to the directory containing the hash files for verifiying the new firmwares                                                                                                                                          	| string    	|                    	| By default the hash_file_location uses the value of firmware_dir 	|
| hash_file_name     	| The name of the hash file<br><br>**If not specified Ansible will attempt to find a .txt file that has the same name as the firmware it is intending to patch with**                                                               	| string    	|                    	| ""                                                               	|
| new_firmware_name  	| The name of the new firmware<br><br>The variable is only utilized if **operation_mode** = **manual**                                                                                                                          	| string    	|                    	| ""                                                               	|
| new_firmware_hash  	| The MD5 hash of the new firmware<br><br>The variable is only utilized if **operation_mode** = **manual**                                                                                                                      	| string    	|                    	| ""                                                               	|

Dependencies
------------

  - scp version 0.10.2 and above (https://pypi.org/project/scp)

Example Playbook
----------------

```
- name: Upload firmware in to cisco catalyst switches based on their model numbers
  hosts: all
  gather_facts: false
  vars:
    ansible_network_os: ios

  tasks:

    - name: Get switch details
      ios_facts:

    - include_role:
        name: automation.patcher.ios.push_firmware
        apply:
          vars:
            firmware_dir: "{{ playbook_dir }}/firmware/{{ ansible_net_model }}"
            ansible_command_timeout: 3200
```

License
-------

BSD

Author Information
------------------

Alex Neo Jing Hui, Contact me at me@alexneo.net
