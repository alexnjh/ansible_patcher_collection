automation.patcher.clean_switch
=========

This role contains all the relevant tasks for cleaning up of firmware to Cisco Catalyst switches

Requirements
------------

- Ansible version 2.8 and above.

Role Variables
--------------

| **Variable**            	| **Description**                                                                                                                                      	| **Data type** 	| **Avaliable options** 	| **Default value** 	|
|-------------------------	|------------------------------------------------------------------------------------------------------------------------------------------------------	|---------------	|-----------------------	|-------------------	|
| excluded_file_list      	| A list of files to be excluded from deletion from the switch                                                                                         	| list          	|                       	| []                	|
| ansible_command_timeout 	| A magic variable in ansible that sets the duration to wait in seconds before terminating a command.<br><br>Set this higher if facing timeout errors. 	| int           	|                       	| 3200              	|                                                      	|

Dependencies
------------


Example Playbook
----------------

```
- name: Clean-up unused files 
  hosts: all
  gather_facts: false
  vars:
    ansible_network_os: ios

  tasks:

    - include_role:
        name: automation.patcher.ios.clean_switch
        apply:
            become: true
```

License
-------

BSD

Author Information
------------------

Alex Neo Jing Hui, Contact me at me@alexneo.net

