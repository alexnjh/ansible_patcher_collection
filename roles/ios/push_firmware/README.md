automation.patcher.push_ios_firmware
=========

This role contains all the relevant tasks for uploading/pushing of firmware to Cisco Catalyst switches

Requirements
------------

- Ansible version 2.8 and above.

Role Variables
--------------

| **Variable**             | **Description**                                                                                                                                                                                                                                     | **Data type** | **Available options** | **Default value** |
|--------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------|-----------------------|-------------------|
| firmware_dir             | The directory that contains the new firmware that will be uploaded to the switch. <br><br>The string can either be a relative or absolute path.<br><br>If there are multiple firmware found in the directory, the latest firmware will be uploaded. | string        |                       | firmware          |
| use_records              | Enable the usage of a records file to keep track of the firmware that was uploaded to the switches by Ansible.<br><br>Setting this to true allows ansible to keep track of which firmware was uploaded to the switches.                             | boolean       | - true<br>- false     | true              |
| records_file             | The path to the records file. The string can be either a relative or absolute path. <br><br>This variable will be ignored if **use_records** is set to false.                                                                                       | string        |                       | push_records.txt  |
| delete_previous_firmware | Ansible will delete previously uploaded firmware before uploading a newer firmware to the switch.<br><br>This variable will be ignored if **use_records** is set to false.                                                                          | boolean       | - true<br>- false     | true              |
| scp_timeout              | Set the number of seconds to wait while the file is being transferred from the switch to ansible. Set a relative high number if there are a lot of files to be transferred.                                                                         | int           |                       | 3600              |
| hash_file_location              | The path to a file that contains a list of hashes in Ansible variable format for Ansible to utilized for verifying firmware files                                                                        | string           |                       | vars/hash_file.yml              |

Dependencies
------------

  - scp version 0.10.2 and above (https://pypi.org/project/scp)

Example Playbook
----------------

```
- name: Upload firmware to cisco catalyst switches
  hosts: all
  roles:
      - { role: automation.patcher.push_ios_firmware, firmware_dir: "{{ playbook_dir }}/firmware" }  
  gather_facts: false
  vars:
    ansible_network_os: ios

```

or

```
- name: Backup cisco catalyst switches
  hosts: all
  gather_facts: false
  vars:
    ansible_network_os: ios

  tasks:
    - include_role:
        name: automation.patcher.push_ios_firmware
        apply:
          vars:
            firmware_dir: "{{ playbook_dir }}/firmware"
```

License
-------

BSD

Author Information
------------------

Alex Neo Jing Hui, Contact me at me@alexneo.net
